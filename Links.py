from google.appengine.ext import db

class Link(db.Model):
    desc = db.StringProperty(required=True)
    href = db.StringProperty(required=True)

    @staticmethod
    def getAll():
        q = db.Query(Link)
        return q
