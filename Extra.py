from google.appengine.ext import db

class Extra(db.Model):
    description = db.TextProperty(required=True)
