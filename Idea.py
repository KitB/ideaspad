import web
from google.appengine.ext import db

from enum import Enum

from User import ensureLogin, getCurrentUser
from Extra import Extra
from Form import Form

class A:
    def __init__(self, desc):
        self.description=desc

Stages = Enum('Untouched', 'Started', 'Stagnant', 'Finished')
StageArgs = []
for stage in Stages:
    StageArgs.append((int(stage), stage))

class Idea(db.Model):
    stage = db.IntegerProperty(required=True)
    description = db.TextProperty(required=True)
    Stage = Stages
    changeForm = Form( web.form.Dropdown(name="stage", args=StageArgs)
                              , web.form.Button("Yup")
                              , web.form.Hidden(name="idea")
                              )
    extraForm = Form( web.form.Textarea(name="description", description="")
                             , web.form.Button("Add extra")
                             , web.form.Hidden(name="idea")
                             )
    delForm = Form( web.form.Button("x")
                           , web.form.Hidden(name="idea")
                           )

    def getExtras(self):
        q = db.Query(Extra)
        q.ancestor(self)
        return q

    def getLink(self):
        return self.key().name() or self.key().id()

    def render(self):
        changeForm = self.changeForm()
        changeForm.fill({'idea': self.key().id(), 'stage':self.stage})

        extraForm = self.extraForm()
        extraForm.fill({'idea': self.key().id()})

        delForm = self.delForm()
        delForm.fill({'idea': self.key().id()})

        return render.idea_detail(self, changeForm, extraForm, delForm)

    def deleteWithChildren(self):
        q = db.Query(Extra)
        q.ancestor(self)
        for e in q:
            e.delete()
        self.delete()

render = web.template.render('templates/', globals={'stages':Idea.Stage})

def get_idea(key_or_id):
    user = ensureLogin()
    idea = db.get(db.Key.from_path('User', user.user_id(), 'Idea', key_or_id))
    if not idea:
        try:
            idea = Idea.get_by_id(long(key_or_id), parent=getCurrentUser())
        except ValueError:
            pass
    return idea


def get_ideas(user):
    q = db.Query(Idea)
    q.ancestor(user)
    q.order('stage')
    return q
