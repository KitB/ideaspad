from web.template import CompiledTemplate, ForLoop, TemplateResult


# coding: utf-8
def idea_detail (idea, changeForm, extraForm, delForm):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    self['title'] = join_(escape_(idea.description, True))
    extend_([u'<div>\n'])
    extend_([u'    <p class="idea_stage_', escape_(idea.stage, True), u'">', escape_(idea.description, True), u'</p>\n'])
    extend_([u'    <form action="delete" method="post">\n'])
    extend_([u'        ', escape_(delForm.render(), False), u'\n'])
    extend_([u'    </form>\n'])
    extend_([u'    <form action="update" method="post">\n'])
    extend_([u'        ', escape_(changeForm.render(), False), u'\n'])
    extend_([u'    </form>\n'])
    extend_([u'    <ul class="idea_extra">\n'])
    for extra in loop.setup(idea.getExtras()):
        extend_(['    ', u'<li>\n'])
        extend_(['    ', u'    ', escape_(extra.description, True), u'\n'])
        extend_(['    ', u'</li>\n'])
    extend_([u'    </ul>\n'])
    extend_([u'    <form action="addextra" method="post">\n'])
    extend_([u'        ', escape_(extraForm.render(), False), u'\n'])
    extend_([u'    </form>\n'])
    extend_([u'</div>\n'])

    return self

idea_detail = CompiledTemplate(idea_detail, 'templates/idea_detail.html')
join_ = idea_detail._join; escape_ = idea_detail._escape

# coding: utf-8
def admin (form):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    self['title'] = join_(u'admin')
    extend_([u'\n'])
    extend_([escape_(form.render(), False), u'\n'])

    return self

admin = CompiledTemplate(admin, 'templates/admin.html')
join_ = admin._join; escape_ = admin._escape

# coding: utf-8
def base (page):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    extend_([u'<!doctype html>\n'])
    extend_([u'<html>\n'])
    extend_([u'    <head>\n'])
    extend_([u'        <title>', escape_(page.title, True), u'</title>\n'])
    extend_([u"        <link href='http://fonts.googleapis.com/css?family=Ubuntu+Mono|Ubuntu' rel='stylesheet' type='text/css' />\n"])
    extend_([u'        <link rel="stylesheet" href="styles/main.css" />\n'])
    extend_([u'    </head>\n'])
    extend_([u'    <body>\n'])
    extend_([u'        <div id="content">\n'])
    extend_([u'            ', escape_(page, False), u'\n'])
    extend_([u'            <div id="footer">\n'])
    extend_([u'                <a href="/">index</a> | <a href="', escape_(logout_url(), True), u'">logout</a>\n'])
    for link in loop.setup(getAllLinks()):
        extend_(['                ', u' | <a href="', escape_(link.href, True), u'">', escape_(link.desc, True), u'</a>\n'])
    extend_([u'            </div>\n'])
    extend_([u'        </div>\n'])
    extend_([u'    </body>\n'])
    extend_([u'    <script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>\n'])
    extend_([u'    <script type="text/javascript" src="js/mathjaxconfig.js"></script>\n'])
    extend_([u'</html>\n'])

    return self

base = CompiledTemplate(base, 'templates/base.html')
join_ = base._join; escape_ = base._escape

# coding: utf-8
def input_idea (title, form):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    self['title'] = join_(escape_(title, True))
    extend_([u'\n'])
    extend_([u'<form action="" method="post">\n'])
    extend_([u'    ', escape_(form.render(), False), u'\n'])
    extend_([u'</form>\n'])

    return self

input_idea = CompiledTemplate(input_idea, 'templates/input_idea.html')
join_ = input_idea._join; escape_ = input_idea._escape

# coding: utf-8
def index (title, content):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    self['title'] = join_(escape_(title, True))
    extend_([u'\n'])
    extend_([escape_(content, False), u'\n'])

    return self

index = CompiledTemplate(index, 'templates/index.html')
join_ = index._join; escape_ = index._escape

# coding: utf-8
def ideas (title, ideas, form):
    __lineoffset__ = -4
    loop = ForLoop()
    self = TemplateResult(); extend_ = self.extend
    self['title'] = join_(escape_(title, True))
    extend_([u'\n'])
    extend_([u'<h2>Here are your ideas so far:</h2>\n'])
    extend_([u'\n'])
    extend_([u'<ul>\n'])
    for idea in loop.setup(ideas):
        extend_([u'<a href="', escape_(idea.getLink(), True), u'"><li class="idea_stage_', escape_(idea.stage, True), u'">\n'])
        extend_([u'    ', escape_(idea.description, True), u'\n'])
        extend_([u'</li></a>\n'])
    extend_([u'</ul>\n'])
    extend_([u'\n'])
    extend_([u'<form action="" method="post">\n'])
    extend_([u'    ', escape_(form.render(), False), u'\n'])
    extend_([u'</form>\n'])

    return self

ideas = CompiledTemplate(ideas, 'templates/ideas.html')
join_ = ideas._join; escape_ = ideas._escape

