#!/usr/bin/env python2
# Copyright (c) 2012, Kit Barnes
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do
# so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
logging.getLogger().setLevel(logging.DEBUG)

import web

from google.appengine.ext import db

from Idea import Idea, get_idea, get_ideas
from User import ensureLogin, ensureAdmin, getCurrentUser, getLoginURL, getLogoutURL
from Extra import Extra
from Links import Link
from Form import Form, CombinedForm

render = web.template.render('templates/', base='base', globals={'login_url':getLoginURL, 'logout_url':getLogoutURL, 'getAllLinks':Link.getAll})

def getForm():
    return Form( web.form.Textbox( name='description', description="new idea", size="60" )
               , web.form.Button( 'Have Idea' )
               )

class index:
    form = getForm()
    def GET(self):
        user = ensureLogin()
        if user:
            return render.ideas("%s ideas" % user.nickname(), get_ideas(getCurrentUser()), self.form())

    def POST(self):
        user = ensureLogin()
        if user:
            form = self.form()
            if not form.validates():
                return "Form didn't validate"
            i = Idea( description=form.d.description
                    , parent=db.Key.from_path('User', user.user_id())
                    , stage=int(Idea.Stage.Untouched)
                    )
            i.put()
            web.seeother("/")


class idea:
    form = getForm()
    def GET(self, ideaStr):
        user = ensureLogin()
        idea = get_idea(ideaStr)
        if idea:
            return render.index(ideaStr, idea.render())
            #return render.idea_detail(idea)
        else:
            form = self.form()
            return render.input_idea(ideaStr, form)

    def POST(self, ideaStr):
        user = ensureLogin()
        form = self.form()
        if not form.validates():
            return "Form didn't validate"
        i = Idea( key_name=ideaStr
                , description=form.d.description
                , stage=int(Idea.Stage.Untouched)
                , parent=db.Key.from_path('User', user.user_id())
                )
        i.put()
        web.seeother("/%s" % ideaStr)

# TODO: Consolidate the following three objects into an object with a parameter
#       (Look at all that duplication! UGH)

class update:
    def POST(self):
        user = ensureLogin()
        form = Idea.changeForm()
        if not form.validates():
            return "Form didn't validate"
        logging.debug(form.d.idea)
        i = Idea.get_by_id(long(form.d.idea), parent=getCurrentUser())
        logging.debug(i)
        i.stage = int(form.d.stage)
        i.put()
        web.seeother("/%s" % form.d.idea) # XXX: Bug where numeric *named* ideas override ids

class addextra:
    def POST(self):
        user = ensureLogin()
        form = Idea.extraForm()
        if not form.validates():
            return "Form didn't validate"
        i = Idea.get_by_id(long(form.d.idea), parent=getCurrentUser())
        e = Extra( description=form.d.description
                 , parent=i
                 )
        e.put()
        web.seeother("/%s" % form.d.idea)

class delete:
    def POST(self):
        user = ensureLogin()
        form = Idea.extraForm()
        if not form.validates():
            return "Form didn't validate"
        i = Idea.get_by_id(long(form.d.idea), parent=getCurrentUser())
        i.deleteWithChildren()
        web.seeother("/")

class admin:
    nlf = Form( web.form.Textbox(description="Description", name="desc"), web.form.Textbox("href"), web.form.Button("Add Link") )

    def GET(self):
        user = ensureAdmin()
        newLinkForm = CombinedForm( [self.nlf()], action="", method="post" )
        return render.admin(newLinkForm)

    def POST(self):
        user = ensureAdmin()
        f = self.nlf()
        if not f.validates():
            return "Form didn't validate"
        l = Link(**f.d)
        l.put()
        web.seeother("/admin")

urls = ( '/', index
       , '/update', update
       , '/addextra', addextra
       , '/delete', delete
       , '/admin', admin
       , '/(.*)', idea
       )

app = web.application(urls, globals())
application = app.wsgifunc()
