import web
import logging

from google.appengine.api import users
from google.appengine.ext import db

def ensureUser(user):
    user_exists = User.gql('where user = :1', user).get()
    if not user_exists:
        logging.debug("Creating user %s" % user.nickname())
        new_user = User(user=user, key_name=user.user_id())
        new_user.put()

def ensureLogin():
    user = users.get_current_user()
    if user:
        ensureUser(user)
        return user
    else:
        raise web.seeother(getLoginURL())

def ensureAdmin():
    user = ensureLogin()
    if users.is_current_user_admin():
        return user
    else:
        raise web.seeother("/")

def getLoginURL():
    return users.create_login_url(web.ctx.get('path', '/'))

def getLogoutURL():
    return users.create_logout_url('/')

def getCurrentUser():
    user = users.get_current_user()
    return db.get(db.Key.from_path('User', user.user_id()))

class User(db.Model):
    user = db.UserProperty()
